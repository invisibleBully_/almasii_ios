//
//  PreviewImageViewController.swift
//  AlmasiiApp
//
//  Created by Junior on 29/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PreviewImageViewController: UIViewController {
    
    
    var imageURL: String!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var downloadErrorImageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }


    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    
    func initData(imageURL: String){
        self.imageURL = imageURL
        setFeedImage(url: self.imageURL)
    }
    
    func setFeedImage(url: String){
        Alamofire.request(url).responseImage { (response) in
            if let feedImage = response.result.value {
                self.feedImageView.image = feedImage
                self.activityIndicator.isHidden = true
                self.downloadErrorImageView.isHidden = true
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.downloadErrorImageView.isHidden = false
                return
            }
        }
    }
}
