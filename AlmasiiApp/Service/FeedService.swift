//
//  FeedService.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON


class FeedService {
    
    static let instance = FeedService()
    var feeds = [Feed]()
    var selectedFeed: Feed?
    var sortedFeeds = [Feed]()

    
    
    func fetchFeeds(completion: @escaping CompletionHandler){
        Alamofire.request(FEED_URL, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                do{
                    if let json = try JSON(data: data).array{
                        for feed in json{
                            let imageURL = feed["url"].stringValue
                            let dowloadToken = feed["downloadToken"].stringValue
                            let imageName = feed["name"].stringValue
                            let createdAtMillis = Int(feed["createdAtMillis"].stringValue)
                            
                            let feed = Feed(imageURL: imageURL, downloadToken: dowloadToken, imageName: imageName, createdAtMillis: createdAtMillis)
                            self.feeds.append(feed)
                        }
                        self.feeds = self.feeds.sorted(by: {$0.createdAtMillis > $1.createdAtMillis})
                        completion(true)
                    }
                }catch{
                    completion(false)
                }
            }else{
                completion(false)
            }
        }
    }
}
