//
//  Constant.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import Foundation


typealias CompletionHandler = (_ Success: Bool) -> ()
let FEED_URL = "https://us-central1-instanew-60742.cloudfunctions.net/allPhotos"
let CHECK_CONNECTIVITY_MSG = "Please make sure you have an active internet connection."
