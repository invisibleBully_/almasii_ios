//
//  Feed.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import Foundation

struct Feed {
    public private(set) var imageURL: String!
    public private(set) var downloadToken: String!
    public private(set) var imageName: String!
    public private(set) var createdAtMillis: Int!
}
