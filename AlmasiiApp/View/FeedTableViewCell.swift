//
//  FeedTableViewCell.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire


class FeedTableViewCell: UITableViewCell {

    
    //MARK - Outlets
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var feedDate: UILabel!
    let imageCache = NSCache<AnyObject, AnyObject>()
    @IBOutlet weak var imageDownloadErrorImageView: UIImageView!
    @IBOutlet weak var imageActivityIndicator: UIActivityIndicatorView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.separatorInset = UIEdgeInsets(top: 20, left: 20, bottom: 5, right: 20);
        self.layer.borderWidth = 5;
        self.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.layer.cornerRadius = 5.0
    }

    
    
   
    
    
    func configureCell(feed: Feed){
        let imageURLString = feed.imageURL
        if let imageFromCache = imageCache.object(forKey: feed.imageURL as AnyObject) as? UIImage {
            self.feedImageView.image = imageFromCache
            return
        }
        self.imageActivityIndicator.isHidden = false
        self.imageActivityIndicator.startAnimating()
        Alamofire.request(feed.imageURL).responseImage { (response) in
            if let feedImage = response.result.value {
                let imageToCache = feedImage
                if imageURLString == feed.imageURL {
                    self.feedImageView.image = imageToCache
                }
                self.imageCache.setObject(imageToCache, forKey: feed.imageURL as AnyObject)
                self.imageDownloadErrorImageView.isHidden = true
                self.imageActivityIndicator.isHidden = true
                self.imageActivityIndicator.stopAnimating()
            }else{
                self.imageDownloadErrorImageView.isHidden = false
                self.imageActivityIndicator.isHidden = true
                self.imageActivityIndicator.stopAnimating()
                return
            }
           
        }
        
        
        let date = Date(timeIntervalSince1970: (TimeInterval(feed.createdAtMillis / 1000)))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
        self.feedDate.text = String(describing: dateFormatter.string(from: date))
    }
    
    
    override func prepareForReuse() {
        self.feedImageView.image = nil
    }
    
}
