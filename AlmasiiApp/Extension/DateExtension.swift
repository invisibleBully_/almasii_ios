//
//  DateExtension.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import Foundation


typealias UnixTime = Int


extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

