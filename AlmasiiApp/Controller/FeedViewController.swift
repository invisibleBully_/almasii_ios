//
//  FeedViewController.swift
//  AlmasiiApp
//
//  Created by Junior on 28/09/2018.
//  Copyright © 2018 almasii. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl?
    @IBOutlet weak var mainIndicator: UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        addRefreshControl()
        mainIndicator.startAnimating()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if !Connectivity.isConnectedToInternet {
            mainIndicator.stopAnimating()
            mainIndicator.isHidden = true
          alertUser()
        }else{
             fetchFeed()
        }
    }
    
 

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FeedService.instance.feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as? FeedTableViewCell{
            cell.configureCell(feed: FeedService.instance.feeds[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFeed = FeedService.instance.feeds[indexPath.row]
        let imagePreview = PreviewImageViewController()
        imagePreview.modalPresentationStyle = .custom
        imagePreview.initData(imageURL: selectedFeed.imageURL)
        present(imagePreview, animated: true, completion: nil)
    }
    
    
    
    func addRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
        refreshControl?.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        FeedService.instance.feeds = []
        fetchFeed()
        refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    
    
    func fetchFeed(){
        FeedService.instance.fetchFeeds { (success) in
            if success {
                self.tableView.reloadData()
                self.mainIndicator.isHidden = true
                self.mainIndicator.stopAnimating()
            }else{
                self.mainIndicator.stopAnimating()
                self.mainIndicator.isHidden = true
                return
            }
        }
    }
    
    
    func alertUser(){
        let alert = UIAlertController(title: "Alert", message: CHECK_CONNECTIVITY_MSG, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

