# AlmasiiFeed - iOS

AlmasiiFeed is a demo iOS app that makes a web service request to create a photo feed in a reverse chronological order.

# Functionalities

 * Pull down to refresh
 * Image preview



## Getting Started
These instructions will get you a copy of the project up and running on your local machine for further development and testing purposes. 



### Prerequisites
* XCode with Deployment Target (iOS 12.0) - Will run on simulators 5S and newer(Latest Xcode Build)
* Latest XCode Build Tools
 


### Installing
* Clone repository into desired location
* Open "AlmasiiApp.xcworkspace" due to external library use
* Clean and build project, compile and run on simulator of choice or select build target from general settings and set to desired target when running on actual device.

***. NB: Firebase returns a 402 error code on the URLs sometimes due to a limit on the APIs usage. This prevent images from loading but all other data like date appears.



## Built With

* [Alamofire](https://github.com/Alamofire/Alamofire)
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage)
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) 

